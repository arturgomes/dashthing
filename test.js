
var request = require('supertest');
var should = require('should');
var app  = require('./app');
var port = 3333;

var baseUrl = 'http://localhost:' + port;
var server;

describe('app', function () {

  before(function (done) {
    server = app.listen(port, function (err, result) {
      if (err) {

        process.exit(1);
        done(err);
      } else {
        done();
      }
    });
  });
 
  after(function (done) {
    server.close();
    done();
    process.exit(0);
  });


  it('GET /', function (done) {
    request(baseUrl)
      .get('/')
      .set('Accept', 'application/json')
      .expect('Content-Type', "text/html; charset=utf-8")
      .expect(200, done)
  });

});
